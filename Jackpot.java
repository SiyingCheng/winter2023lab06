public class Jackpot{
	public static void main (String[] args){
	
	System.out.println("Welcome to play Jackpot! ");
	
	Board board = new Board();
	boolean gameOver = false;
	int numOfTilesClosed = 0;
		
		while (!gameOver) {
            System.out.println(board.toString());
            if (board.playATurn()) {
                gameOver = true;
            } 
			else {
                numOfTilesClosed++;
            }
        }
		
	if (numOfTilesClosed>=7){
		System.out.println("Reached JackPot. You Win!");
	}
	else {
		System.out.println("Sorry,you lose!");
	}
	
	}	
}