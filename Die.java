import java.util.Random;
public class Die {
	private int faceValue;
    private Random random;

    public Die() {
        this.faceValue = 1;
        this.random = new Random();
    }

    public int getFaceValue() {
        return this.faceValue;
    }
	// die has a value from 1-6
    public void roll() {
        this.faceValue = this.random.nextInt(6) + 1;
    }

    public String toString() {
        return " " + this.faceValue;
    }
}
