public class Board{
	private Die dieOne;
	private Die dieTwo;
	private boolean[] tiles;

    public Board() {
        this.dieOne = new Die();
        this.dieTwo = new Die();
        this.tiles = new boolean[12];
    }
	
	public String toString(){
		String empty = "";
		
		for (int i=0;i<this.tiles.length;i++)
		{
		if (!tiles[i]){
			empty += (i+1) + " ";
		}
		else {
			return empty += "X";
		}
		}
		return empty;
		
	}
	
	public boolean playATurn() {
        this.dieOne.roll();
		this.dieTwo.roll();
		
		System.out.println(""+this.dieOne.toString());
		System.out.println(""+this.dieTwo.toString());
		
		int sumOfDice = this.dieOne.getFaceValue() + this.dieTwo.getFaceValue();
		if (!tiles[sumOfDice-1]){
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		else if (!tiles[this.dieOne.getFaceValue()-1]){
			tiles[this.dieOne.getFaceValue()-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}	
		else if (!tiles[this.dieTwo.getFaceValue()-1]){
			tiles[this.dieTwo.getFaceValue()-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		else {
            System.out.println("All the tiles for these values are already shut");
            return true;
        }
	}
}